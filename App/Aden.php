<?php

//namespace App\Base;

//use Core\Traffic;
//use Core\Routes;

class Aden{

    private $routes=[];
    private $dynamic=[];
    private $cleanURL;
    private $style;
    private $request;
    private $router;

    public function __construct($style=true){
       $this->style = $style;
       $this->request = new Requests();
       $this->router  = new Router();
       $this->cleanURL = $this->request->getHttpRequest()->url;
    }

    public function routes($routes=[]){

        if($this->style){
            foreach ($routes[$this->request->getMethod()] as $route => $callback) {
                if(!$this->request->isDync($route)){
                $this->routes[$this->request->getMethod()][$route] = $callback;
                }else{
                    $this->dynamic[$this->request->getMethod()][$route] = $callback;
                }
            }
         return $this;
        }else{
            echo 'you should change param to false in constructor';
        }
    }

    

    public function engage(){
     
        if(array_key_exists($this->cleanURL,$this->routes[$this->request->getMethod()])){
            switch (gettype($this->routes[$this->request->getMethod()][$this->cleanURL])) {
                case 'object':
                           echo call_user_func($this->routes[$this->request->getMethod()][$this->cleanURL]);
                    break;
                case 'string':
                        if(strpos($this->routes[$this->request->getMethod()][$this->cleanURL],'@')!==false){
                            echo 'This is controller';
                        }else{
                            echo 'This is not a controller';
                        }
                break;

                case 'array':
                             echo 'Array to handle';
                    break;

                default:
                             return false;
                    break;
            }
            
        }elseif(!empty($this->dynamic)){
            $this->router->dynamicRouteHandler($this->dynamic,$this->request->getMethod(),$this->cleanURL);
        }else{
            echo 'Not found';
        }
        
    }
}
?>