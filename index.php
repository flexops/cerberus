<?php

//require_once 'vendor/autoload.php';
//use App\Base;

require_once 'Core/Router.php';
require_once 'Core/Requests.php';
require_once 'App/Aden.php';


$app = new Aden();

$app->routes([
                  'GET'=>[
                          '/'=>function(){echo 'Hello world';},
                          '/about'=>function(){echo 'Hi About';},
                          '/contact'=>function(){echo 'Hi contact';},
                          '/users'=>'users@add',
                          '/handle'=>[],
                          '/user/{id}'=>function($args){echo $args['id'];},
                          '/user/{id}/{name}'=>function($args){echo $args['id'].'--'.$args['name'];}
                  ],
                  'POST'=>[
                        '/me'=>function(){echo 'Hi ME';}
                  ],
                  'PUT'=>[
                        '/update'=>function(){echo 'Hi Update';}
                  ],
                  'DELETE'=>[
                        '/del'=>function(){echo 'Hi Del';}
                  ]
                 ])->engage();



?>