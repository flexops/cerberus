<?php

//namespace Core\Traffic;

class Requests{
    
    private $request =[];

    public function __construct()
    {
        $url = explode('/',trim($_SERVER['REQUEST_URI'],'/'));
        unset($url[0]);
        $reUrl = implode('/',$url);
        $this->request['url'] = !empty($reUrl)?'/'.$reUrl:'/';
        $this->request['method'] = $_SERVER['REQUEST_METHOD'];
        $this->request['header'] = (object)getallheaders();
    }


    public function getMethod(){
    return $this->request['method']; 
    }

    public function getHttpRequest(){
        return (object)$this->request;
    }

    public function isDync($route){
        if(strpos($route,'{')!==false && strpos($route,'}')!==false){
          return true;
        }
      }
}


?>